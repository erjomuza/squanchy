#ifndef UI_H
#define UI_H

#include "Player.h"
#include <string>
#include <iostream>
#include <fstream>
#include <stdlib.h>


using namespace std;

//! Circulo.h
/*!
  Esta clase hereda de la clase Figura, se encuentra al mismo nivel de las clases Triangulo y Rectangulo.
*/
class UI {

public:
    //! El constructor de la clase.
    /*!
    */
    UI();
    //! El constructor con parametros.
    /*!
    */
    void save();
    void load();
    void insterpretador(string lect);
    Player * jugador_1;
    Player * jugador_2;

    string gameSave;
    string salida;
    string gameName;
    ~UI();


private:
protected:
};

#endif /* UI_H */
