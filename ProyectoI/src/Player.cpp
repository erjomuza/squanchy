#include "Player.h"

int Player::plCount = 0;
Player::Player(){
  Player::plCount++;
  this->idPlayer=Player::plCount;
}
Player::Player(Field * a, int maxC){
  this->playfield = a;
  this->maxCost = maxC;
  Player::plCount++;
  this->idPlayer=Player::plCount;
}

Player::~Player(){

}

void Player::createArmy(int A, int C, int L){
    if (L*3+C*5+A*4 > this->maxCost) {
      cout << "Excede el maximo" << '\n';
      return;
    }
    this->army = new Unit*[A+C+L];
    for (size_t i = 0; i < A; i++) {
      this->army[i]= new Archery;
    }
    for (size_t i = A; i < C+A; i++) {
      this->army[i]= new Cavalry;
    }
    for (size_t i = C+A; i < C+A+L; i++) {
      this->army[i]= new Lancer;
    }
    this->armySize=A+C+L;
}
void Player::addUnit(int posX, int posY, char type, int hitPoints, int unitNumb){
    if (type == 'A') {
      this->army[unitNumb] = new Archery(posX, posY, hitPoints, unitNumb);
      this->army[unitNumb]->move(this->playfield->playfield, this->army[unitNumb]->posX, this->army[unitNumb]->posY);
    }
    if (type == 'C') {
      this->army[unitNumb] = new Cavalry(posX, posY, hitPoints, unitNumb);
      this->army[unitNumb]->move(this->playfield->playfield, this->army[unitNumb]->posX, this->army[unitNumb]->posY);
    }
    if (type == 'L') {
      this->army[unitNumb] = new Lancer(posX, posY, hitPoints, unitNumb);
      this->army[unitNumb]->move(this->playfield->playfield, this->army[unitNumb]->posX, this->army[unitNumb]->posY);
    }
}

void Player::play(){
    char a;
    int x_move;
    int y_move;
    bool mov;
    Cell ** celdaAso;
    celdaAso = this->playfield->playfield;
    for (size_t i = 0; i < armySize; i++) {
      if (this->army[i] == NULL) {
        continue;
      }
      cout << "Jugador " << this->idPlayer <<'\n';
      cout << "Unit" << this->army[i]->name << " " << i << " x = "<< this->army[i]->posX<< " y = "<< this->army[i]->posY  <<'\n';
      cout << "move?(y or n)" << '\n';
      cin >> a;
      if (a=='y') {
        cout << "where in x?" << '\n';
        cin >> x_move;
        cout << "where in y?" << '\n';
        cin >> y_move;
        if (x_move == -1 && y_move == -1) {
          continue;
        } else{
          mov = this->army[i]->move(celdaAso, x_move, y_move);
        }
        if (!mov) {
          cout << "No se puede mover a dicha posicion, intente de nuevo" << '\n';
          i--;
        }
      }
    }
}

void Player::posUnits(){
  int x_move;
  int y_move;
  bool mov;
  Cell ** celdaAso = this->playfield->playfield;
  if (this->idPlayer == 1) {
    cout << "Puede poner unidades desde la columna 1 hasta " << this->playfield->cols/4 +1 << '\n';
  }else{
  cout << "Puede poner unidades desde la columna " << this->playfield->cols - this->playfield->cols/4 << "hasta " <<this->playfield->cols<< '\n';
  }
  for (size_t i = 0; i < armySize; i++) {
    cout << "Unit" << this->army[i]->name << " " << i << '\n';
    cout << "Adonde in x?" << '\n';
    cin >> x_move;
    cout << "Adonde in y?" << '\n';
    cin >> y_move;
    ///Estas asignaciones son necesarias ya que
    ///move necesita que la posicion este dentro del rango
    ///entonces hacer esto porque en un inicio las unidades
    ///No tienen valores asignados a x y y
    this->army[i]->posX=x_move;
    this->army[i]->posY=y_move;
    mov = this->army[i]->move(celdaAso, x_move, y_move);
    if (mov==false) {
      cout << "No se puede mover a dicha posicion, intente de nuevo" << '\n';
      i--;
    }
  }
}
void Player::randomPlace(){
  int minReach;
  int reach;
  int randX;
  int randY;
  bool mov;
  if (this->idPlayer == 1) {
    minReach=1;
    reach = this->playfield->cols/4;
  }else{
    minReach=this->playfield->cols - this->playfield->cols/4;
    reach = this->playfield->cols;
  }
  srand(time(NULL));
  cout << rand() << '\n';
  //std::cout << "X "<< randX << " " << "Y "<<randY << '\n';
  for (size_t i = 0; i < this->armySize; i++) {
    randY = rand()%(reach - minReach+1) + minReach;
    randX = rand()%(this->playfield->rows)+1;
    ///Estas asignaciones son necesarias ya que
    ///move necesita que la posicion este dentro del rango
    ///entonces hacer esto porque en un inicio las unidades
    ///No tienen valores asignados a x y y
    this->army[i]->posX=randX;
    this->army[i]->posY=randY;
    mov = this->army[i]->move(this->playfield->playfield, randX, randY);
    if (!mov) {
      i--;
    }
  }

}
