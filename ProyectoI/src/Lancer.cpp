#include "Lancer.h"


Lancer::Lancer(){
  this->type='L';
  this->name="Lancero";
  this->maxHitPoints=20;
  this->hitPoints=20;
  this->attackPoints=7;
  this->defense=7;
  this->range=1;
  this->level=1;
  this->experience=0;
  this->movement=1;
  this->cost=3;
}
Lancer::Lancer(int x, int y, int vida, int unitNumb){
  this->type='L';
  this->name="Lancero";
  this->maxHitPoints=20;
  this->hitPoints=vida;
  this->attackPoints=7;
  this->defense=7;
  this->range=1;
  this->level=1;
  this->experience=0;
  this->movement=1;
  this->cost=3;
  this->posX=x;
  this->posY=y;
  this->id=unitNumb;
}

Lancer::~Lancer(){

}
